(function(ng) {
    function ToDoController($scope, Title) {
        this.title = Title;
        this.isShow = true;
        this.todos = [
            { id: 1, title: 'Cras justo odio', isComplete: true },
            { id: 2, title: 'Dapibus ac facilisis in', isComplete: false },
            { id: 3, title: 'Morbi leo risus', isComplete: false },
            { id: 4, title: 'Porta ac consectetur ac', isComplete: false },
            { id: 5, title: 'Vestibulum at eros', isComplete: true },
        ];
    }

    ToDoController.prototype.fixMe = function(e) {
        this.title = 'ToDo List';
        console.log(e);
    }

    ToDoController.prototype.onBlur = function(e) {
        console.log(e);
    }

    ToDoController.prototype.addToDo = function(e) {
        e.preventDefault();
        this.todos.push({ id: this.todos.length, title: this.title, isComplete: false });
        this.title = '';
    }

    ToDoController.prototype.remove = function(e, id) {
        e.preventDefault();
        const index = this.todos.findIndex(function(item) {
            return item.id === id;
        });
        this.todos.splice(index, 1);
    }

    ToDoController.prototype.save = function(e) {
        delete todo.$edit;
    }

    ng.module('app', [])
        .value('Title', 'Home ToDo')
        .controller("ToDoController", ToDoController)
})(angular);